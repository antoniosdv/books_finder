var mongo = require('mongodb');
 
var Server = mongo.Server,
    Db = mongo.Db,
    BSON = mongo.BSONPure;
 
var server = new Server('localhost', 27017, {auto_reconnect: true});
var db = new Db('booksdb', server, {safe: false});
 
db.open(function(err, db) {
    if(!err) {
        console.log("Connected to 'booksdb' database");
    }
});
 
exports.findById = function(req, res) {
    var id = req.params.id;
    console.log('Retrieving book: ' + id);
    db.collection('books', function(err, collection) {
        collection.findOne({'_id':new BSON.ObjectID(id)}, function(err, item) {      
        	console.log(item)  	
            res.send(decorateItem(item));
        });
    });
};
 
exports.findAll = function(req, res) {
    db.collection('books', function(err, collection) {
        collection.find().toArray(function(err, items) {
            res.send(decorateItems(items));
        });
    });
};
 
exports.addBook = function(req, res) {
    var book = req.body;
    console.log('Adding book: ' + JSON.stringify(book));
    db.collection('books', function(err, collection) {
    	console.log(collection)
        collection.insert(book, {safe:true}, function(err, result) {
            if (err) {
                res.send({'error':'An error has occurred'});
            } else {
                console.log('Success: ' + JSON.stringify(result[0]));
                res.send(decorateItem(result[0]));
            }
        });
    });
}
 
exports.updateBook = function(req, res) {
    var id = req.params.id;
    var book = req.body;
    console.log('Updating book: ' + id);
    console.log(JSON.stringify(book));
    db.collection('books', function(err, collection) {
        collection.update({'_id':new BSON.ObjectID(id)}, book, {safe:true}, function(err, result) {
            if (err) {
                console.log('Error updating book: ' + err);
                res.send({'error':'An error has occurred'});
            } else {
                console.log('' + result + ' document(s) updated');
                res.send(decorateItem(book));
            }
        });
    });
}
 
exports.deleteBook = function(req, res) {
    var id = req.params.id;
    console.log('Deleting book: ' + id);
    db.collection('books', function(err, collection) {
        collection.remove({'_id':new BSON.ObjectID(id)}, {safe:true}, function(err, result) {
            if (err) {
                res.send({'error':'An error has occurred - ' + err});
            } else {
                console.log('' + result + ' document(s) deleted');
                res.send(req.body);
            }
        });
    });
}

function decorateItem(item) {
	if(item && item._id) {
        item.id = item._id;
        delete item._id;
    }
	return item;
}

function decorateItems(items) {
	items.forEach(decorateItem);
	return items;
}