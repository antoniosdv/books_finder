var express = require('express'),
	path = require('path'),
	bodyParser = require('body-parser')
    books = require('./routes/books');
 
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/books', books.findAll);
app.get('/books/:id', books.findById);
app.post('/books', books.addBook);
app.put('/books/:id', books.updateBook);
app.delete('/books/:id', books.deleteBook);

app.listen(3000);
console.log('Listening on port 3000...');
