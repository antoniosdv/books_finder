$(function() {

    // ------------- MODELS -----------------
    
    var Book = Backbone.Model.extend({

        urlRoot: '/books',

        defaults: {
            title: null,
            language: null,
            edition: null,
            publisher: null
        }

    });
    
    var Books = Backbone.Collection.extend({

        url: '/books',
        
        model: Book,

        initialize: function() {
            this.on('change', this.sortIfComparatorExists, this);
        },

        sortIfComparatorExists: function() {
            this.comparator && this.sort();
        },

        sortByAttr: function(attr) {
            this.comparator = attr;
            this.sort();
        }
        
    });
    
    // ------------- DECORATORS -----------------
    
    function FilteredCollection(original){
        var filtered = new original.constructor();
    
        var getSafeRegExpFromQuery = function(query) {
            var parsedQuery = 
                _.chain(query.split(''))
                .map(function(char) { 
                    return (new RegExp(/[a-z0-9 ]/i)).test(char) ? char : "\\" + char 
                })
                .value()
                .join('');
            return new RegExp(parsedQuery, 'i');
        }

        var filterByQuery = function(collection, query) {
            var re = getSafeRegExpFromQuery(query);
            return collection.filter(function(model) {
                return re.test(model.get('title')) || re.test(model.get('language')) || false;   
            });            
        };
        
        filtered.search = function(query) {      
            if(query === filtered._query) return;      
            filtered._query = query || null;   
            var items = filtered._query ?  filterByQuery(original, filtered._query) : original.models;
            filtered.reset(items);
        };
        
        filtered.getQuery = function() {
            return filtered._query || null;
        };

        filtered.listenTo(original, "reset sync", function() {
            filtered.search(filtered.getQuery() || null);
        });    

        return filtered;
    }
    
    // ------------- VIEWS -----------------
    
    var FilterView = Backbone.Marionette.ItemView.extend({
        
        template: '#filter-template',
        
        ui: {
            filter:    'input'
        },

        events: {
            'keyup @ui.filter':     'onSearch'
        },
        
        onRender: function() {
            this.collection.getQuery() && this.ui.filter.val(this.collection.getQuery());
        },

        onSearch: function() {
            App.vent.trigger('books:search', this.ui.filter.val());
        }
        
    });
    
    
    var RowView = Backbone.Marionette.ItemView.extend({
       
        tagName: 'tr',
        
        template: '#row-template',

        events: {
            'click td' :    'onRowClick'
        },

        onRowClick: function() {            
            App.vent.trigger('books:form:show', this.model.id);
        }
        
    });

    var TableView = Backbone.Marionette.CompositeView.extend({
        
        template: '#table-template',

        childView: RowView,

        childViewContainer: 'tbody',
        
        events: {
            'click thead .sort':     'onSort'  
        },

        collectionEvents: {
            'sort':     'render'
        },
        
        onRender: function() {
            this.markSortedCol();
        },

        markSortedCol: function() {
            if(typeof this.collection.comparator === 'string') {
                this.$('th[data-sort_by=' + this.collection.comparator + ']').addClass('sorted');
            }
        },

        onSort: function(evt) {
            var th = $(evt.target).closest('th');
            if(th.hasClass('sorted')) return;
            th.siblings('.sorted').removeClass('sorted');
            this.collection.sortByAttr(th.addClass('sorted').data('sort_by'));
        }
        
    });
    
    var ListView = Backbone.Marionette.LayoutView.extend({   

        template: '#list-template', 

        regions: {
            filter: '#filter',
            table:  '#table'
        },
        
        onRender: function() {
            this.filter.show(new FilterView({collection: this.collection}));
            this.table.show(new TableView({collection: this.collection}));
        }
        
    });

    var FileView = Backbone.Marionette.ItemView.extend({  

        template: '#file-template',

        ui: {
            buttons : '.btn',
            form : 'form'
        },

        events: {
            'click .cancel' :      'onCancelClick',
            'click .save' :        'onSaveClick'
        },

        bindDataToModel: function() {
            _.each(this.ui.form.serializeArray(), function(el) {
                this.model.set(el.name, el.value);
            },this);
            return this.model;
        },

        disableButtons: function() {
            this.ui.buttons.prop("disabled", true);
        },

        onCancelClick: function(evt) {
            evt.preventDefault();
            this.disableButtons();
            App.vent.trigger('books:form:cancel');
        },

        onSaveClick: function(evt) {
            evt.preventDefault();
            this.disableButtons();
            this.bindDataToModel().save().done(this.onSaveDone);
        },         
    
        onSaveDone: function() {
            App.vent.trigger('books:form:saved');
        }

    });
    
    // ------------- BUSINESS -----------------

    var Business = {

        loadBooks: function() {
            var load = new $.Deferred();
            if(this.books) return load.resolve(this.books);
            var allBooks = new Books();
            this.books = FilteredCollection(allBooks);
            allBooks.fetch().done(_.bind(function() {
                load.resolve(this.books);
            }, this));
            return load.promise();
        },

        loadBook: function(id) {
            var load = new $.Deferred();
            if(this.books && this.books.get(id)) return load.resolve(this.books.get(id));
            var book = new Book({id: id});            
            book.fetch().done(function() {
                load.resolve(book);
            });
            return load.promise();
        },

        getBooks: function() {
            return this.books;
        },
        
        booksSearch: function(query) {
            this.getBooks().search(query);
        }

    };

    // ------------- CONTROLLERS -----------------

    var EventsController = {

        searchIfQuery: function(query, triggerAction) {
            Backbone.history.navigate('books' + (query ? '/search/' + query : ''), triggerAction);
        },

        attach: function() {
            App.vent.on('books:form:show', this.onFormShow, this);
            App.vent.on('books:form:cancel', this.returnToList, this);
            App.vent.on('books:form:saved', this.returnToList, this);       
            App.vent.on('books:search', this.onSearch, this);  
        },

        returnToList: function() {
            this.searchIfQuery(Business.getBooks() && Business.getBooks().getQuery(), true);
        },

        onFormShow: function(id) {
            Backbone.history.navigate('books/' + id, {trigger: true});
        },

        onSearch: function(query) {
            Business.booksSearch(query);
            this.searchIfQuery(query);
        }

    };

    var MainController = {

        showList: function(query) {
            $.when(Business.loadBooks()).done(function(books) {
                if(query) Business.booksSearch(query);
                App.content.show(new ListView({collection: books}));
            });
        },

        showFile: function(id) {            
            $.when(Business.loadBook(id)).done(function(book) {
                App.content.show(new FileView({model: book}));
            });
        }

    };

    // ------------- ROUTER -----------------

    var Router = Backbone.Marionette.AppRouter.extend({

        controller: MainController,

        appRoutes: {
            'books':                'showList',
            'books/:id':            'showFile',
            'books/search/:query':  'showList',
            '':                     'showList'
        }

    });

    // ------------- START APPLICATION -----------------

    var App = new Backbone.Marionette.Application();
    App.addRegions({content: '#content'});
    App.addInitializer(function() {
        new Router();
        EventsController.attach();
        Backbone.history.start();
    });
    App.start();

});